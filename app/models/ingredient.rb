class Ingredient < ApplicationRecord
    # has_many :mycals, dependent:   :destroy
    # has_many :users, through: :mycals
    has_many :mycals, dependent:   :destroy
    has_many :user, through: :mycals
    validates :ingredient_name, presence: true, length: { maximum: 50 }
    validates :weight, numericality: { greater_than_or_equal_to: 0 }
    validates :calorie, numericality: { greater_than_or_equal_to: 0 }
    
    def self.search(search)
        return Ingredient.where('ingredient_name LIKE ?', "%#{search}%")
    end
end
