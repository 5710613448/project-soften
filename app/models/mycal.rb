class Mycal < ApplicationRecord
    # belongs_to :user
    belongs_to :ingredient
    
    validates :owner_id, presence: true
    validates :ingredient_id, presence: true
end
