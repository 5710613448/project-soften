class SearchController < ApplicationController
  
  def home
    if params[:search]
      @ingredients = Ingredient.all
      @isSearch = true
      @results = Ingredient.search(params[:search])
      if params[:inid] && params[:owid]
        param = {owner_id: params[:owid], ingredient_id: params[:inid]}
        @suc = Mycal.new(param)
        @suc.save
      end
    else  
      @ingredients = Ingredient.all
      @isSearch = false
    end 
  end
  
end
