class MycalController < ApplicationController

  def show
  end
  
  def index
    @results = Mycal.where(owner_id: session[:user_id])
    @inge = Ingredient.all
    @total_weight = 0
    @total_calorie = 0
    @results.each do |res|
      @inge.each do |ing|
        if res.ingredient_id === ing.id
          @total_weight += ing.weight
          @total_calorie += ing.calorie
        end
      end
    end
  end
  
  def destroy
    if params[:id]
      @mycal = Mycal.find(params[:id])
      @mycal.destroy
      redirect_to mycal_index_path
    else
      @mycals = Mycal.where(owner_id: session[:user_id])
      @mycals.each do |mycalss|
        mycalss.destroy
      end
      redirect_to mycal_index_path
    end
  end

end
