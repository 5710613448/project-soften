json.extract! ingredient, :id, :ingredient_name, :weight, :calorie, :created_at, :updated_at
json.url ingredient_url(ingredient, format: :json)
