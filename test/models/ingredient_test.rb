require 'test_helper'

class IngredientTest < ActiveSupport::TestCase
  
  def setup
    @ingredient = Ingredient.new(ingredient_name: "Hamburger", weight: "100",
              calorie: "500")
  end
  
  test "should be valid" do
    assert @ingredient.valid?
  end
  
  test "ingredient name should be present" do
    @ingredient.ingredient_name = ""
    assert_not @ingredient.valid?
  end
  
  test "ingredient name should not be too long" do
    @ingredient.ingredient_name = "a"*51
    assert_not @ingredient.valid?
  end
  
  test "weight should not be negative number" do
    @ingredient.weight = -1
    assert_not @ingredient.valid?
  end
  
  test "calorie should not be negative number" do
    @ingredient.calorie = -1
    assert_not @ingredient.valid?
  end
  
end
