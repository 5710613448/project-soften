require 'test_helper'

class MycalTest < ActiveSupport::TestCase
  
  def setup
    @mycal = Mycal.new(owner_id: users(:thanapon).id,
                                     ingredient_id: ingredients(:hamburger).id)
  end

  test "should be valid" do
    assert @mycal.valid?
  end

  test "should require a owner_id" do
    @mycal.owner_id = nil
    assert_not @mycal.valid?
  end

  test "should require a ingredient_id" do
    @mycal.ingredient_id = nil
    assert_not @mycal.valid?
  end
  
  
end
