class CreateMycals < ActiveRecord::Migration[5.2]
  def change
    create_table :mycals do |t|
      t.integer :owner_id
      t.integer :ingredient_id

      t.timestamps
    end
    add_index :mycals, :owner_id
    add_index :mycals, :ingredient_id
  end
end
