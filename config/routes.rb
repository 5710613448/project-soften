Rails.application.routes.draw do
  
  get 'mycal/index'
  root 'sessions#new'
  
  get 'welcome/index'
  get '/register', to: 'users#new'
  get '/about', to: 'welcome#about'
  get '/search', to: 'search#home'
  get '/login', to: 'sessions#new'
  post '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'
  get '/index_ingredient', to: 'ingredients#index'
  delete '/mycal', to: 'mycal#destroy'
  
    resources :users
    resources :ingredients
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
